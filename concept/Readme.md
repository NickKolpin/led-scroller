LED Scroller Concept
====================

* Wall of PCBs.
* PCBs approx 5 x 5 cm^2.
* 4 x 4 LEDs on each board.
* Random arrangement of colours.
* Shift register to control LED lighting.
* Each edge of the PCB has a header for jumper wires that give:
    * GND
    * 3.3 / 5 V
    * data in/out
    * clock
    * LED enable?
* One edge is the input, the other three are potential outputs.
* Input edge has female header, outputs have male.
* Boards are connected using male-female jumpers.
* One or more microcontrollers (on dev boards?) control display.
* Controllers read data in from USB serial and send into first shift register(s).
* Data propogates from one PCB to any connected to it through the chain of shift registers.
* Users can walk up and reconfigure how the PCBs are connected to each other.
* Users can also enter designs (through a web interface?) that are displayed.

![](https://bitbucket.org/NickKolpin/led-scroller/raw/master/concept/scroller.gif)
