Nick's initial design ideas
===========================

I think that getting a working scroller shouldn't be too hard.
Making it cheap will be the difficult part.
PCBs can be made fairly cheaply at hackvana.com (c. 1 GBP / 5x5 cm^2 board)
SMT LEDs and resistors are pretty cheap but could cost c. 1 GBP / board
Shift registers can be cheap. Mostly they're 8 outputs, so two would be needed per board. Perhaps c. 50 p / board.
With headers and jumper cables I would guess another 1 GBP / board.
This gives a total of, say, 4 GBP / board.
I think the display could look cool with at least 10 boards (the more the merrier), so perhaps a budget of 50+ GBP.

