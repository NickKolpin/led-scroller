Overview
========

![](https://bitbucket.org/NickKolpin/led-scroller/raw/master/concept/scroller.gif)

This project involves building a number of small PCBs containing a matrix of LEDs. 
The PCBs can be connected together and the images displayed on them scroll from one PCB to the next.
The aim is to produce a wall mounted display that looks cool.
Users would be able to reconfigure the positions of the various PCBs and connect them in different ways.
They would also be able to design images to be displayed on the LED matrices.
The project was initially an idea for Oxford Hackspace's Artweeks event.
This is a public project and anyone is welcome to work on it.


